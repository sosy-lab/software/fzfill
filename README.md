<!--
SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# FZFill - Fuzzy search and fill Uni2Work CSVs

This little script is intended to help with transferring grades to Uni2Work.

# Installation

- You **need [fzf](https://github.com/junegunn/fzf)** to be installed on your system.
- Install the requirements with `pip install -r requirements.txt`

# Good to Know

- The original input file will not be overwritten, it just serves as the source of students names
- The output file just contains the newly added students

# Usage

```
usage: fzfill [-h] [-o OUTPUT] [-d {,,;,:,|,-,tab,null,space}] csv

Easily fill grading CSVs.

positional arguments:
  csv                   the input csv file (from uni2work)

options:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        the filled output file
  -d {,,;,:,|,-,tab,null,space}, --delimiter {,,;,:,|,-,tab,null,space}
```

# Questions, Bugs or suggestions?

Just write me on **[Zulip](https://chat.ifi.lmu.de/#narrow/stream/287-FZFill)**.