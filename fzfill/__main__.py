# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import sys

sys.dont_write_bytecode = True

from .fzfill import main  # noqa E402

sys.exit(main(sys.argv[1:]))
