# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
import csv
from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime
from functools import partial
from pathlib import Path
from typing import Any, Callable, Iterable, Optional, Sequence, Union
from typing_extensions import Protocol
from pyfzf import FzfPrompt

fzf = FzfPrompt()


@dataclass(frozen=True)
class Student:
    first_name: str
    surname: str
    name: str
    id_number: int

    @property
    def search_key(self):
        return f"{self.name} {self.id_number}"

    def __repr__(self):
        return self.search_key

    def __hash__(self) -> int:
        return self.id_number


def parse_cli(args: Sequence[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser("fzfill", description="Easily fill grading CSVs.")

    parser.add_argument(
        "csv", action="store", type=Path, help="the input csv file (from uni2work)"
    )

    prefix = datetime.strftime(datetime.now(), "%Y-%m-%d_%H-%M")

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        type=Path,
        help="the filled output file",
        default=Path(f"./{prefix}-fzoutput.csv"),
    )

    parser.add_argument(
        "-d",
        "--delimiter",
        action="store",
        choices=[",", ";", ":", "|", "-", "tab", "null", "space"],
        default=",",
    )

    parser.add_argument(
        "--voided",
        action="store_true",
        help="only mark students as voided (bypass task entering)",
    )
    parser.set_defaults(is_update=False)

    subparsers = parser.add_subparsers()
    pupdate = subparsers.add_parser("update", help="update existing records")
    pupdate.add_argument(
        "--exec",
        action="store",
        help="a python expression producing the new value; may use x as the old value",
        default="x",
    )
    pupdate.add_argument("--task", action="store", required=True)
    pupdate.set_defaults(is_update=True)

    return parser.parse_args(args)


def get_idxs_of_tasks(header: list[str]) -> list[int]:
    occ = header.index("occurrence")
    result = header.index("exam-result")

    return list(range(occ + 1, result))


def make_result_class(header: list[str]):
    task_idxs = get_idxs_of_tasks(header)

    print(f"Number of tasks found: {len(task_idxs)}")
    for task in task_idxs:
        print(header[task])
    task_names = tuple((header[i] for i in task_idxs))

    global Result
    Result = namedtuple(  # type: ignore
        "Result",
        task_names,
        rename=True,
    )
    Result.task_names = task_names  # type: ignore


def update(
    result: "Result",  # type: ignore
    task: str,
    updatefn: Callable[[Optional[Union[float, int]]], Optional[Union[float, int]]],
) -> "Result":  # type: ignore
    idx = Result.task_names.index(task)  # type: ignore
    arg = {(task_id := f"_{idx}"): updatefn(getattr(result, task_id))}
    return result._replace(**arg)


def parse_csv(
    path: Path, args: argparse.Namespace
) -> dict[Student, Optional["Result"]]:  # type: ignore
    with path.open("r") as csvfd:
        parsed_csv = list(csv.reader(csvfd, delimiter=args.delimiter))

    header = parsed_csv.pop(0)

    name_idx = header.index("name")
    id_idx = header.index("matriculation")
    surname_idx = header.index("surname")
    first_name_idx = header.index("first-name")

    make_result_class(header)

    task_idxs = [header.index(task) for task in Result.task_names]  # type: ignore

    def extract_result(row: list[Any]):
        results: list[Optional[float]] = []
        for idx in task_idxs:
            try:
                results.append(float(row[idx]))
            except ValueError:
                results.append(None)

        return Result(*results)  # type: ignore

    return {
        Student(
            first_name=row[first_name_idx],
            surname=row[surname_idx],
            name=row[name_idx],
            id_number=int(row[id_idx]),
        ): extract_result(row)
        for row in parsed_csv
    }


def _promt(lookup: dict[str, Student], choices: Iterable[str]):
    p = fzf.prompt(choices)
    try:
        return lookup[p[0]]
    except KeyError:
        print("No student found")
    except IndexError:
        print("No student selected.")


def get_write_header():
    return (
        "first-name",
        "surname",
        "name",
        "matriculation",
        *Result.task_names,
        "exam-result",
    )


def make_row(student: Student, result: "Result"):  # type: ignore
    if -1 in result:
        return [
            student.first_name,
            student.surname,
            student.name,
            student.id_number,
            *[None for _ in result],
            "voided",
        ]
    return [
        student.first_name,
        student.surname,
        student.name,
        student.id_number,
        *result,
        None,
    ]


def _write_out(config: argparse.Namespace, results: dict[Student, "Result"]):  # type: ignore
    outfile = config.output
    tmp_path = outfile.parent / f"tmp_{outfile.name}"
    og_name = outfile.name
    if outfile.exists():
        print(f"{outfile.name} exists saving it as {tmp_path}")
        outfile.rename(tmp_path)

    try:
        with outfile.open("w") as fd:
            writer = csv.writer(fd, delimiter=config.delimiter)
            writer.writerow(get_write_header())
            row_gen = (
                make_row(student, result)
                for student, result in filter(
                    lambda x: x[1] is not None, results.items()
                )
            )
            writer.writerows(row_gen)
            fd.flush()

    except Exception as e:
        print("Exception occurred during write, restoring last write")
        outfile.rename(outfile.parent / og_name)
        raise e

    if tmp_path.exists():
        print("Unlinking ", tmp_path)
        tmp_path.unlink()


def main(args: Sequence[str]):
    config = parse_cli(args=args)
    result_map = parse_csv(config.csv, config)
    lookup = {student.search_key: student for student in result_map.keys()}

    write_out = partial(_write_out, config)
    prompt = partial(_promt, lookup)
    n_tasks = len(Result.task_names)  # type: ignore
    try:
        for n in range(len(result_map)):
            print(f"{n} students entered.")
            student = prompt(result_map.keys())

            if student is None:
                cont = input("done? ")
                if cont.lower().startswith("y"):
                    write_out(result_map)
                    return
                else:
                    continue

            remaining_tasks = n_tasks
            result_input: list[Union[float, int]] = []

            if config.voided:
                result_input = [-1 for _ in range(n_tasks)]
                remaining_tasks = 0

            if config.is_update:
                previous = result_map[student]
                result_input = update(
                    previous, config.task, lambda x: eval(config.exec, {"x": x})
                )
                remaining_tasks = 0

            while remaining_tasks:
                try:
                    result_input.append(
                        float(input(f"Task {n_tasks - remaining_tasks + 1}: "))
                    )
                except ValueError:
                    print("Task values need to be integers")
                    continue
                remaining_tasks -= 1

            result = Result(*result_input)  # type: ignore
            print(student, result)

            result_map[student] = result

            if n > 1 and n % 5 == 0:
                print("Writing...")
                write_out(result_map)
    except KeyboardInterrupt:
        write_out(result_map)
        return
    finally:
        write_out(result_map)
